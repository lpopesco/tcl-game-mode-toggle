# TCL Game Mode Toggle

Simple script to toggle game mode on and off on your TCL TV.

Uses the RokuTV API

Usage: `./toggle-gamemode.sh <TV IP>`

Tested on 55R617
