#!/bin/bash
TV_IP="${1}:8060/"
TV_ENDPOINT="${TV_IP}/keypress/"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
FLAG_FILE="${DIR}/on.flag"


function press_key {
    curl -m 2 -d '' "${TV_ENDPOINT}/${1}"  || exit 1
}

# Open Menu
press_key Info
# Open Picture Settings
press_key Up
press_key Right
# Set Game Mode
for((i=0;i<2;i++)); do
	press_key Up
done
sleep 0.1
press_key Right
#Close Menu
press_key Info

if [ -e  $FLAG_FILE ]
then
    rm -f $FLAG_FILE
else
    touch $FLAG_FILE
fi